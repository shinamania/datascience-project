
from pyspark.sql import SparkSession, Window
from pyspark.sql import SQLContext
from pyspark.sql import functions as F
from pyspark.sql.functions import monotonically_increasing_id

spark = SparkSession.builder \
    .master('local') \
    .appName('Best Performance') \
    .getOrCreate()

sc = spark.sparkContext

sqlContext = SQLContext(sc)

BASE1 = "/Users/shina/data/modelled"
airportsPlanedate = sqlContext.read.parquet(f'{BASE1}/airportsPlanedate').withColumn("id", monotonically_increasing_id())
carriersOTP = sqlContext.read.parquet(f'{BASE1}/CarriersOTP').withColumn("id", monotonically_increasing_id())
airportsPlanedate.registerTempTable("airportsPlanedate")
carriersOTP.registerTempTable("carriersOTP")

#Which carrier performs better - The carrier with the lowest average Delay
#USe ArrTime and DepTime , let's us try the number of flights since we do not have sufficient data
sqlContext.sql("select UniqueCarrier, count(*) as counter from carriersOTP group by UniqueCarrier").sort("counter").show()


#Average delay per day of the week
sqlContext.sql("select DayOfWeek, avg(CarrierDelay + WeatherDelay + NASDelay + "
                         "SecurityDelay + LateAircraftDelay) as AverageDelay from airportsPlanedate group by DayOfWeek")\
    .sort("AverageDelay").show()

#Average delay per time or day of the year
sqlContext.sql("select DayOfMonth, avg(CarrierDelay + WeatherDelay + NASDelay + "
                         "SecurityDelay + LateAircraftDelay) as AverageDelay from airportsPlanedate group by DayOfMonth").sort("AverageDelay").show()

#Average delay per time of the day
sqlContext.sql("select CRSDepTime, (CarrierDelay + WeatherDelay + NASDelay + SecurityDelay + LateAircraftDelay) as sum from airportsPlanedate a, carriersOTP c "
               "where a.id = c.id ").dropna().groupBy("CRSDepTime").agg({"sum": "mean"}).sort("avg(sum)").show()

#Average delay per day of the week
sqlContext.sql("select TailNum,sum(CarrierDelay + WeatherDelay + NASDelay + "
                         "SecurityDelay + LateAircraftDelay) as sumDelay, count(*) from airportsPlanedate a, carriersOTP c where a.id = c.id group by TailNum").show()


#The correlation between weather delay and flight delay
sqlContext.sql("select WeatherDelay, (CarrierDelay+ WeatherDelay  + NASDelay + "
                         "SecurityDelay + LateAircraftDelay) as TotalFlightDelay from airportsPlanedate").dropna()\
    .stat.corr("WeatherDelay", "TotalFlightDelay")
