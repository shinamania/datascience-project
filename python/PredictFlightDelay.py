from pyspark.ml.classification import LogisticRegression
from pyspark.mllib.regression import LabeledPoint
from pyspark.sql import SparkSession, Window
from pyspark.sql import SQLContext
from pyspark.sql import functions as F
from pyspark.sql.functions import monotonically_increasing_id


def parse(l):
  l = [float(x) for x in l]

  return LabeledPoint(l[0], l[1:])


spark = SparkSession.builder \
    .master('local') \
    .appName('Best Performance') \
    .config("spark.sql.autoBroadcastJoinThreshold", -1) \
    .getOrCreate()

sc = spark.sparkContext
sqlContext = SQLContext(sc)

BASE1 = "/Users/shina/data/modelled"
airportsPlanedate = sqlContext.read.parquet(f'{BASE1}/airportsPlanedate').withColumn("id", monotonically_increasing_id())
carriersOTP = sqlContext.read.parquet(f'{BASE1}/CarriersOTP').withColumn("id", monotonically_increasing_id())
airportsPlanedate.registerTempTable("airportsPlanedate")
carriersOTP.registerTempTable("carriersOTP")

# Merge the 2 tables
# Group by flight number and compute mean as dataset - by computing mean and see if you can normalize the data -
# the goal is to reduce the impact of outliers over the entire dataset

data = sqlContext.sql("select FlightNum, mean(CRSDepTime), mean(CarrierDelay), mean(WeatherDelay), mean(NASDelay), "
               "mean(SecurityDelay), mean(LateAircraftDelay) from airportsPlanedate a, carriersOTP c "
               "where a.id = c.id group by FlightNum")

data.show(10)

data.rdd.map(lambda l: parse(l[0])).take(20)
print(f"dataset size: {data.count()}")

lr = LogisticRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)

# Fit the model
(trainingData, testData) = data.randomSplit([0.7, 0.3])
model = lr.fit(trainingData)

# Print the coefficients and intercept for logistic regression
print("Coefficients: " + str(model.coefficients))
print("Intercept: " + str(model.intercept))

trainingSummary = model.summary
trainingSummary.roc.show()
print("areaUnderROC: " + str(trainingSummary.areaUnderROC))

fMeasure = trainingSummary.fMeasureByThreshold
maxFMeasure = fMeasure.groupBy().max('F-Measure').select('max(F-Measure)').head()
bestThreshold = fMeasure.where(fMeasure['F-Measure'] == maxFMeasure['max(F-Measure)']) \
    .select('threshold').head()['threshold']
lr.setThreshold(bestThreshold)

predictions = model.transform(testData)
predictions.select("predictedLabel", "label", "features").show(20)