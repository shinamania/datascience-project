#!/usr/bin/env bash

BASE_DIR=2007-data
i=0
while read line
do
    array[ $i ]="$line"
    (( i++ ))
done < <(ls $BASE_DIR)

for ((i=0; i<=${#array[@]}; i++)); do

INPUT=$BASE_DIR/${array[i]}
OLDIFS=$IFS
IFS=,
[ ! -f  $INPUT ] && { echo "$INPUT file not found"; exit 99; }
echo "Processing & writing to Kafka: $INPUT "
while read Year Month DayofMonth DayOfWeek DepTime CRSDepTime ArrTime CRSArrTime UniqueCarrier FlightNum TailNum ActualElapsedTime CRSElapsedTime AirTime ArrDelay DepDelay Origin Dest Distance TaxiIn TaxiOut Cancelled CancellationCode Diverted CarrierDelay WeatherDelay NASDelay SecurityDelay LateAircraftDelay
do
     echo "$Year,$Month,$DayOfMonth,$DayOfWeek" | /usr/local/Cellar/kafka/0.11.0.1/bin/kafka-console-producer --broker-list localhost:9092 --topic Planedate $1
     echo "$DepTime,$CRSDepTime,$ArrTime,$CRSArrTime,$TaxiIn,$TaxiOut,$AirTime,$ArrDelay,$DepDelay" | /usr/local/Cellar/kafka/0.11.0.1/bin/kafka-console-producer --broker-list localhost:9092 --topic OTP $1
     echo "$UniqueCarrier,$FlightNum,$TailNum" | /usr/local/Cellar/kafka/0.11.0.1/bin/kafka-console-producer --broker-list localhost:9092 --topic Carriers $1
     echo "$Origin,$Dest,$Distance,$Cancelled,$CancellationCode,$Diverted,$CarrierDelay,$WeatherDelays,$NASDelay,$SecurityDelay,$LateAircraftDelay,$ActualElapsedTime,$CRSElapsedTime" | /usr/local/Cellar/kafka/0.11.0.1/bin/kafka-console-producer --broker-list localhost:9092 --topic Airports $1
done < $INPUT
IFS=$OLDIFS
echo "...done"
done


