#!/usr/bin/env bash
./kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Airports
./kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Carriers
./kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Planedate
./kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic OTP