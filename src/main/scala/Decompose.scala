import java.util.UUID

import com.databricks.spark.avro._
import org.apache.spark.sql.SparkSession

object Decompose extends App {

  val BASE = "hdfs://data/decomposed"
  val spark = SparkSession.builder.appName("DataAnalytics StageOne1").master("local[*]").getOrCreate()

  val sqlContext = spark.sqlContext
  println("Loading data...")
  val df2007 = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load("/Users/shina/Downloads/2007.csv")
  val df2008 = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load("/Users/shina/Downloads/2008.csv")
  val df = df2007.union(df2008)

  import org.apache.spark.sql.functions.{lit, udf}

  val generateUUID = udf(() => UUID.randomUUID().toString)

  val airportsPlanedate = df.select("Year", "Month", "DayofMonth", "DayOfWeek", "Origin", "Dest", "Distance", "Cancelled", "CancellationCode", "Diverted", "CarrierDelay",
    "WeatherDelay", "NASDelay", "SecurityDelay", "LateAircraftDelay", "ActualElapsedTime", "CRSElapsedTime").withColumn("Uuid", lit(UUID.randomUUID().toString)).withColumn("TimeStamp", lit(System.currentTimeMillis))
  val carriersOTP = df.select("UniqueCarrier", "FlightNum", "TailNum", "DepTime", "CRSDepTime", "ArrTime", "CRSArrTime", "TaxiIn", "TaxiOut", "AirTime", "ArrDelay", "DepDelay")
    .withColumn("Uuid", lit(UUID.randomUUID().toString)).withColumn("TimeStamp", lit(System.currentTimeMillis))
  println("Loading complete!")
  println("decomposing...")

  airportsPlanedate.show(10)
  carriersOTP.show(10)

  airportsPlanedate.write.avro(s"$BASE/AirportsPlanedate")
  carriersOTP.write.avro(s"$BASE/CarriersOTP")
  println("Done...")
}
