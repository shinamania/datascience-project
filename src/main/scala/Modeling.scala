import com.databricks.spark.avro._
import org.apache.spark.sql.{Column, SparkSession}
import org.apache.spark.sql.functions.{trim, length, when}

object Modeling extends App{

  def emptyToZeroString(c: Column) = when(length(trim(c)) > 0, if(c.equals("null") ||(c == null)) '0' else c).otherwise("0")

    val BASE1 = "hdfs://data/decomposed"
    val BASE2 = "hdfs://data/modelled"
    val spark = SparkSession.builder.appName("DataAnalytics StageOne1").master("local[*]").getOrCreate()
    val sqlContext = spark.sqlContext
    println("Loading data...")

    //Remove duplicate
    val ap_tmp = sqlContext.read.avro(s"$BASE1/AirportsPlanedate").distinct()
    val co_tmp = sqlContext.read.avro(s"$BASE1/CarriersOTP").distinct()

  ap_tmp.show(20)
  co_tmp.show(20)

    //Trim and fix null values
    val airportsPlanedate = ap_tmp.select(
      emptyToZeroString(ap_tmp("Year")) as("Year"),
      emptyToZeroString(ap_tmp("Month")) as("Month"),
      emptyToZeroString(ap_tmp("DayofMonth")) as("DayofMonth"),
      emptyToZeroString(ap_tmp("DayOfWeek"))as("DayOfWeek"),
      emptyToZeroString(ap_tmp("Origin")) as("Origin"),
      emptyToZeroString(ap_tmp("Dest")) as("Dest"),
      emptyToZeroString(ap_tmp("Distance")) as("Distance"),
      emptyToZeroString(ap_tmp("Cancelled")) as("Cancelled"),
      emptyToZeroString(ap_tmp("CancellationCode")) as("CancellationCode"),
      emptyToZeroString(ap_tmp("Diverted")) as("Diverted"),
      emptyToZeroString(ap_tmp("CarrierDelay")) as("CarrierDelay"),
      emptyToZeroString(ap_tmp("WeatherDelay")) as ("WeatherDelay"),
      emptyToZeroString(ap_tmp("NASDelay")) as "NASDelay",
      emptyToZeroString(ap_tmp("SecurityDelay")) as "SecurityDelay",
      emptyToZeroString(ap_tmp("LateAircraftDelay")) as "LateAircraftDelay",
      emptyToZeroString(ap_tmp("ActualElapsedTime")) as "ActualElapsedTime",
      emptyToZeroString(ap_tmp("CRSElapsedTime")) as "CRSElapsedTime",
      ap_tmp("Uuid"),
      ap_tmp("TimeStamp"))

    val carriersOTP = co_tmp.
      select(emptyToZeroString(co_tmp("UniqueCarrier")) as "UniqueCarrier",
        emptyToZeroString(co_tmp("FlightNum")) as "FlightNum",
        emptyToZeroString(co_tmp("TailNum")) as "TailNum",
        emptyToZeroString(co_tmp("DepTime")) as "DepTime",
        emptyToZeroString(co_tmp("CRSDepTime")) as "CRSDepTime",
        emptyToZeroString(co_tmp("ArrTime")) as "ArrTime",
        emptyToZeroString(co_tmp("CRSArrTime")) as "CRSArrTime",
        emptyToZeroString(co_tmp("TaxiIn")) as "TaxiIn",
        emptyToZeroString(co_tmp("TaxiOut")) as "TaxiOut",
        emptyToZeroString(co_tmp("AirTime")) as "AirTime",
        emptyToZeroString(co_tmp("ArrDelay")) as "ArrDelay",
        emptyToZeroString(co_tmp("DepDelay")) as "DepDelay",
        co_tmp("Uuid"),co_tmp("TimeStamp"))

    airportsPlanedate.show(20)
    carriersOTP.show(20)

    airportsPlanedate.write.parquet(s"$BASE2/AirportsPlanedate")
    carriersOTP.write.parquet(s"$BASE2/CarriersOTP")

    println("Done...")
}
