
import java.io.{BufferedOutputStream, File, FileOutputStream, PrintWriter}

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.util.Random

object OTP extends App{

  // Initialization of HDFS writer
  @transient val OTPWriter = new PrintWriter(new File("/Users/shina/data/raw/OTP.csv"))
  OTPWriter.println("DepTime,CRSDepTime,ArrTime,CRSArrTime,TaxiIn,TaxiOut,AirTime,ArrDelay,DepDelay")
  OTPWriter.close()

    val spark = SparkSession.builder.appName("Streaming Carriers & OTP to raw folder").getOrCreate()
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> s"test-consumer-${Random.nextInt(10000)}",
      "auto.offset.reset" -> "earliest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val topic = Array("OTP")
    val streamingContext = new StreamingContext(spark.sparkContext, Seconds(2))
    val stream = KafkaUtils.createDirectStream[String, String](streamingContext, PreferConsistent, Subscribe[String, String](topic, kafkaParams))
    var offsetRanges = Array.empty[OffsetRange]

    stream.foreachRDD { rdd =>
      rdd.map(e => e.value())
        .collect()
        .map(line => {
        val file = new File("/Users/shina/data/raw/OTP.csv")
        val handleWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(file, true)))
        handleWriter.append(s"$line\n")
        handleWriter.close()
      })
    }
    streamingContext.start()
    streamingContext.awaitTermination()
}
